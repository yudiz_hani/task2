const nUser = {
  name: "Your Name",
  address: {
    personal: {
      line1: "101",
      line2: "street Line",
      city: "NY",
      state: "WX",
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
    office: {
      city: "City",
      state: "WX",
      area: {
        landmark: "landmark",
      },
      coordinates: {
        x: 35.12,
        y: -21.49,
      },
    },
  },
  contact: {
    phone: {
      home: "xxx",
      office: "yyy",
    },
    other: {
      fax: '234'
    },
    email: {
      home: "xxx",
      office: "yyy",
    },
  },
};

function getObject(nUser, nParent, nResult = {}) {
  for (let key in nUser) {
    let nName = nParent ? nParent + '_' + key : key;
    if (typeof nUser[key] == 'object') {
      getObject(nUser[key], nName, nResult);
    }
    else {
      nResult[nName] = nUser[key];
    }
  }
  return nResult;
}

var obj = getObject (nUser,"user");
console.log(obj);



